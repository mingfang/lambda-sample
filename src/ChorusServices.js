const axios = require('axios');
const devoliPdcts = require('../config/nz-broadband-products');
const { OrderTypes } = require('./constants');
const { omit } = require('ramda');
const siteOfferEndpoint = 'http://chorus-b2b.devoli.io/product/offer/';
const siteDetailEndpoint = 'http://chorus-b2b.devoli.io/product/site/';
const feasibilityOrderEndpoint = 'http://chorus-b2b.devoli.io/order/feasibility/';
const appointmentEndpoint = 'http://chorus-b2b.devoli.io/order/appointment/';

const allNumbers = RegExp('^[0-9]+$');

const isValidTLC = tlc => !!tlc && allNumbers.test(tlc);

exports.getLocationDetails = async (req, resp) => {
  const { tlc } = req.params;
  if (!isValidTLC(tlc)) {
    resp.status(400).json({ msg: 'Invalid TLC' });
    return;
  }

  const { fibreAvailable, designRequired, buildRequired } = await axios
    .get(siteOfferEndpoint + tlc)
    .then(({ data }) => data.result || {})
    .catch(err => {
      console.log(`Request site offer ${siteOfferEndpoint + tlc} failed with ${err}`);
      throw err;
    });
  const devices = await axios
    .get(siteDetailEndpoint + tlc)
    .then(({ data }) => data.result || {})
    .catch(err => {
      console.log(`Request site product failed with ${err}`);
      throw err;
    });

  return resp.json({
    fibreAvailable,
    designRequired,
    buildRequired,
    products: fibreAvailable ? devoliPdcts : null,
    devices
  });
};

exports.createPreOrder = async (req, resp) => {
  const { action, tlc, pdctId } = req.body;
  const orderType = OrderTypes[action];
  if (!orderType) {
    return resp.status(400).json({ err: 'Unrecognized order type' });
  }

  try {
    const orderResult = await axios
      .post(feasibilityOrderEndpoint, { type: orderType, placeId: tlc, productOfferId: pdctId })
      .then(({ data }) => data.result || {});

    const { siteVisits = [], id: orderId } = orderResult;
    const schedules = await Promise.all(
      siteVisits.map(async ({ id, type }) => {
        const dates = await axios.get(appointmentEndpoint + orderId + '/' + id).then(({ data }) => data.result);
        const datetimes = dates.reduce((all, { start, end }) => {
          const [date, timeStart] = start.split('T');
          const [, timeEnd] = end.split('T');
          const timeSlots = all[date] || [];
          return { ...all, [date]: timeSlots.concat(timeStart + ' - ' + timeEnd) };
        }, Object.create(null));
        return { [type]: datetimes };
      })
    );

    return { ...omit(['siteVisits'], orderResult), schedules };
  } catch (err) {
    return resp.status(400).json({ err: err.toString() });
  }
};

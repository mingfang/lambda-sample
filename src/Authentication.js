const axios = require('axios');
const qs = require('qs');
const { CLIENT_SECRET = '', CLIENT_ID = '', AUTH_SERVER } = process.env;

const retrieveTokens = (uid, pass) =>
  axios.post(
    AUTH_SERVER,
    qs.stringify({
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      grant_type: 'password',
      username: uid,
      password: pass
    }),
    { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
  );

const signIn = async (req, resp) => {
  console.log(CLIENT_ID, CLIENT_SECRET, AUTH_SERVER);
  const { uid, pass } = req.body;
  return retrieveTokens(uid, pass)
    .then(response => {
      console.log(response.data);
      return resp.json(response.data);
    })
    .catch(err => {
      console.log(err);
      const { status, statusText } = err.response || {};
      resp.status(status || 400).json({ msg: statusText || err.toString() });
    });
};

module.exports = {
  signIn
};

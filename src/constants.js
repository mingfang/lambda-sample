exports.OrderTypes = {
  connectPrimary: 'CONNECT_PRIMARY',
  connectAdditional: 'CONNECT_ADDITIONAL_ONT',
  connectSecondary: 'CONNECT_SECONDARY',
  connectAndReplace: 'CONNECT_AND_REPLACE',
  disconnectPrimary: 'DISCONNECT_PRIMARY',
  disconnectSecondary: 'DISCONNECT_SECONDARY',
  changeOffer: 'CHANGE_OFFER',
  modifyAttribute: 'MODIFY_ATTRIBUTE',
  movePrimary: 'MOVE_PRIMARY',
  moveAdditionalONT: 'MOVE_ADDITIONAL_ONT',
  moveAndReplace: 'MOVE_AND_REPLACE',
  transferPrimary: 'TRANSFER_PRIMARY',
  transferSecondary: 'TRANSFER_SECONDARY'
};

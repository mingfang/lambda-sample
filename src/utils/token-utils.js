const jwt = require('jsonwebtoken');

const pemHeader = '-----BEGIN CERTIFICATE-----\n';
const pemFooter = '\n-----END CERTIFICATE-----';

exports.parseToken = (token, pubKey) => {
  try {
    return jwt.verify(token, pemHeader + pubKey + pemFooter);
  } catch (err) {
    console.log('parse token error');
    return null;
  }
};

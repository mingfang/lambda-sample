const { signIn } = require('./src/Authentication');
const { getLocationDetails, createPreOrder } = require('./src/ChorusServices');
const { parseToken } = require('./src/utils/token-utils');
const {
  addressSearch,
  getLocationDetail,
  getInstallDates,
  getRfsDates,
  getScopeDates,
  getProductList
} = require('./src/mock-service');

const app = require('lambda-api')({ version: 'v1.0', base: 'v1' });
const pubKey = process.env.PUB_KEY;

const corsOptions = {
  origin: '*',
  methods: 'GET, PUT, POST, DELETE, OPTIONS',
  headers: 'Content-Type, Authorization, Content-Length, X-Requested-With',
  maxAge: 84000000
};

const verifyTokenMiddleware = (req, resp, next) => {
  const { authorization: authStr } = req.headers;
  const token = authStr.substring(7);
  const user = parseToken(token, pubKey);
  if (user) {
    req.user = user;
    return next();
  }
  return resp.status(400).send('Authentication failed');
};

const Routes = {
  statusCheck: '/status',
  signInPath: '/sign-in',
  locDetailPath: '/chorus/location/:tlc',
  preOrderPath: '/chorus/preorder',
  mockAddrSearch: '/mock/nz/address-search',
  mockLocationDetail: '/mock/nz/location-detail',
  productList: '/mock/nz/product-list',
  scopeDates: '/mock/nz/schedule/scopes',
  installDates: '/mock/nz/schedule/installs',
  rfsDates: '/mock/nz/schedule/rfs'
};

// middleware
app.use(Routes.preOrderPath, verifyTokenMiddleware);
app.use(Routes.locDetailPath, verifyTokenMiddleware);
app.use((_, res, next) => {
  res.cors(corsOptions);
  next();
});

// Routers

app.get(Routes.statusCheck, async (req, rsp) => {
  rsp.status(200);
  return rsp.json({});
});
app.post(Routes.signInPath, signIn);
app.get(Routes.locDetailPath, getLocationDetails);
app.put(Routes.preOrderPath, createPreOrder);

// Mockup data

app.get(Routes.mockAddrSearch, addressSearch);
app.get(Routes.mockLocationDetail, getLocationDetail);
app.get(Routes.productList, getProductList);
app.get(Routes.installDates, getInstallDates);
app.get(Routes.scopeDates, getScopeDates);
app.get(Routes.rfsDates, getRfsDates);

// Preflight OPTION calls
app.options('/*', (req, res) => {
  // Add CORS headers
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  res.status(200).send();
});

exports.handler = async (event, context) => app.run(event, context);
